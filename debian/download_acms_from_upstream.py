#!/usr/bin/python3
import bs4
import requests
import re
import sys
import os
import posixpath
import zipfile
import shutil
import argparse

EXPECTED_LICENSE_URL = "/libs/apps/intel/licenseagreement/US/en/idzlicenseagreements/idzla-sinit-authenticated-code-module-license"
EXPECTED_LICENSE = """

Copyright (c) 2007 - 2011, Intel(R) Corporation.
All rights reserved.
Redistribution
Redistribution and use in binary form, without modification, are permitted
provided that the following conditions are met:

Redistributions must reproduce the above copyright notice and the following disclaimer in the documentation and/or other materials provided with the distribution.
Neither the name of Intel Corporation nor the names of its suppliers may be used to endorse or promote products derived from this software without specific prior written permission.
No reverse engineering, decompilation, or disassembly of this software is permitted.

Limited patent license
Intel Corporation grants a world-wide, royalty-free, non-exclusive license under patents it now or hereafter owns or controls to make, have made, use, import, offer to sell and sell ("Utilize") this software, but solely to the extent that any such patent is necessary to Utilize the software alone. The patent license shall not apply to any combinations which include this software. No hardware per se is licensed hereunder.
DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

SANE_FILENAME_PATTERN = re.compile("^[a-zA-Z0-9_\.-]+$")
SINIT_PATTERN = re.compile("^.*sinit.*\.zip$", re.I)

def get_available_downloads(license_url):
    license_onclick_pattern = re.compile("^downloadLicense\(\d+,'{license_url}'\)$".format(license_url=license_url))
    url = "https://software.intel.com/content/www/us/en/develop/articles/intel-trusted-execution-technology.html"
    r = requests.get(url)
    assert r.status_code == 200
    soup = bs4.BeautifulSoup(r.text, "lxml")
    download_link = {
        "onclick": lambda x: x is not None and license_onclick_pattern.match(x)
    }
    downloads = []
    for a in soup.findAll("a", download_link):
        downloads.append("https://software.intel.com" + a["data-id-url"])
    return downloads

def get_license(license_url):
    url = "https://software.intel.com/{license_url}.html".format(license_url=license_url)

    r = requests.get(url)
    assert r.status_code == 200
    soup = bs4.BeautifulSoup(r.text, "lxml")
    license_div = soup.find("div", {"id": "licenseContentBody"})
    return license_div.text.replace("\r\n", "\n")

def download_acms(urls, target_directory):
    if os.path.exists(target_directory):
        print("Target directory {target_directory} already exists".format(target_directory=target_directory))
        sys.exit(1)
    os.mkdir(target_directory)
    os.mkdir(os.path.join(target_directory, "acm"))
    for url in available_sinit_downloads:
        filename = posixpath.basename(url)
        assert SANE_FILENAME_PATTERN.match(filename)
        r = requests.get(url)
        assert r.status_code == 200
        local_zip_filename = os.path.join(target_directory, "acm", filename)
        assert local_zip_filename.endswith(".zip")
        local_unpack_dir = local_zip_filename[:-4]
        with open(local_zip_filename, "wb+") as f:
            f.write(r.content)
        with zipfile.ZipFile(local_zip_filename, "r") as f:
            f.extractall(local_unpack_dir)
        os.unlink(local_zip_filename)
        
if __name__ == "__main__":
    parser = argparse.ArgumentParser("Download ACMs from Intel website and ensure that license has not changed")
    parser.add_argument("--output-directory", required=True, help="Target directory")
    args = parser.parse_args()
    
    current_license = get_license(EXPECTED_LICENSE_URL)
    if current_license != EXPECTED_LICENSE:
        print("License appears to have changed, please investigate:")
        print(current_license)
        sys.exit(1)

    sinit_filter = lambda x: SINIT_PATTERN.match(x)
    available_sinit_downloads = list(filter(sinit_filter, get_available_downloads(EXPECTED_LICENSE_URL)))
    assert len(available_sinit_downloads) > 5

    download_acms(available_sinit_downloads, args.output_directory)
