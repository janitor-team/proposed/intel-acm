Definition of the TXT.ERRORCODE register for i7_QUAD-SINIT:
==========================================================

Bit   Name        Description
---   ----        -----------
31    Valid       Valid error when set to '1'. The rest of the register
                  contents should be ignored if '0'
30    External    '0' if induced from the processor
                  '1' if induced from software
29:0  Type        This is implementation and source specific. It provides more
                  details on what the step was being performed at the time a
                  failure condition was detected


Type field definition for AC modules:
====================================

Bit     Description
-----   -----------
29:25   Reserved
24:16   TPM command return code, valid only for progress code 0dh and error
        code 1010
24:16   LCP v2 minor error code, valid only for progress code 10h
        24:22   Index (in LCP_POLICY_DATA::PolicyLists) of item responsible
                for error
        21:16   Minor error code (see progress code 10h below)
15      '0' if error generated by AC module (below field definitions apply)
        '1' if generated by other software (field definitions will be
        software-specific)
14:10   AC module error codes
9:4     AC module progress codes
0:3     AC module type
        0000  BIOS
        0001  SINIT
        0010 - 1111 Reserved for future use


SINIT Progress and Error Codes:
==============================
(Error codes of 00000 indicate an unexpected error during the indicated
processing)

Progress   Error
Code       Code      Description
--------   -----     -----------
00h        00000     SINIT Exit Point
           00001     EntryPoint field of MLE header is outside of MLE linear
                     address space
           00100     MLE Join structure is located above 4GB

01h        00000     SINIT Entry Point
           00001     TXT.HEAP.BASE/SIZE registers were not initialized by BIOS
           00010     DPR BASE/SIZE registers were not initialized by BIOS
           00011     TXT heap is above physical memory
           00100     unused
           00101     TXT heap is above 4GB
           00110     TXT heap region is not contained in DPR

02h        00000     Initial Checks
           00001     SINIT does not support platform's chipset device ID
           00010     unused
           00011     unused
           00100     SINIT was not invoked by GETSEC[SENTER]

03h        00000     Start MTRR Check
           00001     DEF_TYPE MTRR not UC or fixed MTRRs not disabled
           00010     variable MTRRs types not WB
           00011     variable MTRRs overlap
           00100     variable MTRR base not a multiple of its size
           00101     variable MTRRs not cover SINIT (rounded up to 4k boundary)
           00110     variable MTRRs not cover single contiguous region
           00111     Invalid MTRR mask value
           01000     SINIT size larger than Authenticated Code Execution
                     Area (ACEA)

04h        00000     Chipset Configuration Testing
           00001     unused
           00010     DPR size too small (< 3MB)
           00011     unused
           00100     TXT heap size incorrect
           00101     TXT heap base not immediately above SINIT region
           00110     SINIT region not completely contained within DPR
           00111     size of SINIT region (TXT.SINIT.SIZE) incorrect (!= 128KB)
           01000     unused
           01001     unused
           01010     unused
           01011     unused
           01100     one base address register (BAR) overlaps another
           01101     DPR register is unlocked
           01110     a base address register (BAR) size is incorrect
           01111     Current values of VT-d PMR registers do not match
                     requested values in SinitMleData
           10000     Specified SINIT address is not located within SINIT
                     region (TXT.SINIT.BASE/SIZE)
           10001     unused
           10000     MCHBAR is above 4GB
           10010     SINIT region is above 4GB

05h        00000     Reading OsSinitData
           00001     version not supported (does not match OsSinitDataVer
                     field of SINIT's chipset information table)
           00010     PMR Low Base or Size not 2MB granular
           00011     PMR Low Base > 4GB
           00100     PMR Low Size > 4GB
           00101     PMR High Base or Size not 2MB granular
           00110     PMR Low fields specify range that overlaps PMR High range
           00111     OsSinitData is outside of the TXT heap
           01000     OsSinitDataSize incorrect
           01001     Requested capabilities (OsSinitData.Capabilites) are not
                     supported
           01010     LCP Data blobs not in DMA-protected memory

06h        00000     Enable TXT Protections
           No error codes currently defined.

07h        00000     Processing MLE Page Tables
           00001     a page directory entry is not contiguous
           00010     a page table entry is not contiguous
           00011     a non-initial entry (PDPE/PDE/PTE) is invalid/not present
                     (i.e. holes in page table are not allowed)
           00100     one of the rules for table address ordering was not met
           00101     unused
           00110     MLE size specified by page table does not match size in
                     OsSinitData
           00111     2MB page sizes not supported
           01000     page overlaps VT-d DMAR table
           01001     page is outside of "good" MDR regions
           01010     page is not covered by DPR nor PMR regions

08h        00000     Registering STM Hash
           No error codes currently defined.

09h        00000     Registering MLE Hash
           No error codes currently defined.

0ah        00000     Building SinitMleData
           00001     could not find RSDP ACPI table
           00010     RSDP ACPI table checksum invalid
           00011     RSDT ACPI table checksum invalid
           00100     could not find VT-d DMAR ACPI table
           00101     VT-d DMAR ACPI table checksum invalid
           00110     BARs in VT-d DMAR DRHD struct mismatch
           00111     VT-d DMAR ACPI table length incorrect
           01000     device scope of VT-d DMAR ACPI table is invalid
           01001     unused
           01010     addresses in VT-d RMRR ACPI table are invalid
           01011     VT-d DMAR ACPI table overlaps DPR
           01100     error with DMAR entry for Azalia engine

0bh        00000     Processing MLE Header Structure
           00001     unsupported version (< 2.0)
           00010     invalid GUID
           00011     FirstValidPage field invalid or does not match first
                     valid page in page table
           00100     MLE does not support SINIT's RLP wakeup capability
           00101     EntryPoint is outside of MLE's linear address range

0ch        00000     MSEG Checking
           00001     unused
           00010     invalid MSEG size
           00011     invalid MSEG base
           00100     inconsistent MSEG base

0dh        00000     TPM_Extend Attempt
           00001     TPM is not ready (TPM_ACCESS_x invalid)
           00010     unable to get access to the locality
           00011     unused
           00100     unused
           00101     unused
           00110     reserved
           00111     reserved
           01000     invalid response from the TPM
           01001     timeout for TPM response
           01010     TPM returned an error (see bits 24:16 for error value)
           01011     TPM NV RAM is unlocked
           01100     TPM is disabled
           01101     TPM is deactivated
           01110     TPM NV AUX and/or PD index incorrectly defined
           01111     TPM PCR 17 was not properly initialized
           10000     TPM PCR 17 extend failed
           10001     TPM PCR 18 extend failed

0eh        00000     SCHECK
           00001     top of low memory (TOLM) is incorrect
           00010     unused
           00011     unused
           00100     memory alias detected
           00101     unused
           00110     top of high memory (TOHM) is incorrect
           00111     unused
           01000     memory configuration is incorrect (SAD)
           01001     memory configuration is incorrect (TSEG)
           01010     memory configuration is incorrect (PCIEX BAR)
           01011     memory configuration is incorrect (MAD overlap)
           01100     memory configuration is incorrect (IIO SAD)
           01101     memory configuration is incorrect (TAD)
           01110     memory configuration is incorrect (SAG)
           01111     memory configuration is incorrect (RIR)
           10000     SMRRs are not set correctly
           10001     memory configuration is incorrect (channel mapping)

0fh        00000     Chipset Protection
           00001     VT-d remap engine enabled
           00010     reserved

10h        00000     Processing Launch Control Policy
           00001     Owner policy is of type LCP_POLTYPE_LIST but no policy
                     data has been provided
           00010     MLE measurement is not in policy
           00011     current platform configuration (PCRs) does not match
                     configuration in policy (LCP_PCONF_ELEMENTs)
           00100     unused
           00101     current SINIT module is revoked by the policy
           00110     One of the AC modules is pre-production and pre-production
                     ACMs are not allowed (or there is no policy)
           00111     unused
             -
           10000     unused
           10001     Owner policy integrity failed (minor code contains
                     specific error)
           10010     Supplier policy integrity failed (minor code contains
                     specific error)
           ------    minor error codes:
           000000    no error
           000001    unsupported policy version
           000010    unsupported hash algorithm
           000011    unsupported policy type
           000100    policy data is too large or at invalid location
           000101    policy hash mismatch
           000110    unsupported RSA key size
           000111    invalid policy data file signature
                     ("Intel(R) TXT LCP_POLICY_DATA\0\0\0\0")
           001000    too many policy lists
           001001    unsupported policy list version
           001010    policy list size does not match accumulated size of
                     individual components (elements)
           001011    unsupported signature algorithm
           001100    policy signature failed to verify
           001101    policy list was revoked
           001110    unsupported policy element hash algorithm
           001111    policy element has incorrect size
           010000    invalid TPM_PCR_INFO_SHORT in LCP_PCONF_ELEMENT

11h        00000     Miscellaneous
           00001     an interrupt or exception occurred
           00010     VT-d DMAR table size exceeds TXT heap space
