History of i5_i7_DUAL-SINIT
===========================

v51:

-  Security update for Dec 2011 Security Advisory ID: INTEL-SA-0003(www.intel.com/security)

-  No changes in v19 - v50, aligning version numbers across platforms

v18:
    
-  Initial release
    
-  Support for v2 of Launch Control Policy
    
-  Supports OsSinitData v5
    
-  Support for SinitMleData v7
    
-  Enforces that LCP Data blobs must reside in DMA-protected memory
    
-  Returns MLE page table base addr in ECX after SENTER
